'use strict';

var Sequelize = require('sequelize');

var sequelize = new Sequelize('cognito', '', '',{
	host: 'localhost',
	dialect: 'postgres'
});

sequelize.authenticate().then(function(err){
	if (err){
		console.log('Unable to connect to database:', err);
	} else{
		console.log('Connection has been established succesfully')
	}
})