var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('Ywpi3LYxdFb5tTPNw_KivQ');

export default function sendTemplate(opts) {
  new Promise(function (resolve, reject) {
    mandrill_client.messages.sendTemplate(opts, resolve, reject)
  })
}

// usage
// sendTemplate({
//   "template_name": template_name,
//   "template_content": template_content,
//   "message": message,
//   "async": async,
//   "ip_pool": ip_pool,
//   "send_at": send_at
// })
// .then(handleSuccess)
// .catch(handleError)