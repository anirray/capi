import express from 'express';
import session from 'express-session';
import redis from 'redis';
var redisStore = require('connect-redis')(session);
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import config from '../src/config';
import passport from 'passport';
import local from './config/passport/local';
import google from './config/passport/google';
import * as actions from './actions/index';
import * as consumer from './actions/third_party_apis/consumer';
import {mapUrl, middleware} from 'utils/url.js';
import PrettyError from 'pretty-error';
import http from 'http';
import SocketIo from 'socket.io';
import models from './db/models';
import moment from 'moment';

const pretty = new PrettyError();
const app = express();

const client = redis.createClient();//CREATE REDIS CLIENT
const server = new http.Server(app);

const io = new SocketIo(server);
io.path('/ws');

app.use(cookieParser('optimusdime'));

//REDIS Session Store Code
app.use(session(
  {
    secret: 'cognitoaylmao',
    store: new redisStore({ host: 'localhost', port: 6379, client: client }),
    saveUninitialized: false, // don't create session until something stored,
    resave: false, // don't save session if unmodified
    cookie: { maxAge: 6000000 }
  }
));

// Initialize passport and also allow it to read
// the request session information.
app.use(passport.initialize());
app.use(passport.session());

// When we give a cookie to the browser, it is just the userId (encrypted with our secret).
passport.serializeUser(function (user, done) {
    done(null, user.id);
});

// When we receive a cookie from the browser, we use that id to set our req.user
// to a user found in the database.
passport.deserializeUser(function(id, done) {
  // query the current user from database
  models.User.find({
    where: {id: id},
    include: [{
      model: models.AccountTypes,
    }]
  })
  .then(function(user){
      done(null, user);
      return null;
  }).error(function(err){
      done(new Error('User ' + id + ' does not exist'));
  });
});

// Use the following Oauth strategies
passport.use(local);
passport.use(google);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Google OAuth Endpoints
app.get('/auth/google', passport.authenticate('google',  { scope : ['profile', 'email'] }));
app.get('/auth/google/callback', passport.authenticate('google', {successRedirect: 'http://localhost:3000', failureRedirect: 'http://localhost:3000'}))

// app.get('/users/getUserById/:id', (req,res) => {
//     middleware(req,res,actions);
// });

// app.get('/users/getUserByName/:id', (req,res) => {
//     middleware(req,res,actions);
// });

app.delete('/users/deleteUser/:id', (req,res) => {
    middleware(req,res,actions);
});

app.put('/users/updateUser/:id', (req,res) => {
    middleware(req,res,actions);
});

app.use((req, res) => {
  const splittedUrlPath = req.url.split('?')[0].split('/').slice(1);

  const {action, params} = mapUrl(actions, splittedUrlPath);

  if (action) {
    action(req, params)
      .then((result) => {
        if (result instanceof Function) {
          result(res);
        } else {
          res.json(result);
        }
      }, (reason) => {
        if (reason && reason.redirect) {
          res.redirect(reason.redirect);
        } else {
          console.error('API ERROR:', pretty.render(reason));
          res.status(reason.status || 500).json(reason);
        }
      });
  } else {
    res.status(404).end('NOT FOUND');
  }
});


const bufferSize = 100;
const messageBuffer = new Array(bufferSize);
let messageIndex = 0;

if (config.apiPort) {

  models.sequelize.sync({}).then(function () {
  console.log('synced: not true, place the following "force: true" in the above object to make it wipe upon reload ')

  const runnable = app.listen(config.apiPort, (err) => {
    if (err) {
      console.error(err);
    }
    console.info('----\n==> 🌎  API is running on port %s', config.apiPort);
    console.info('==> 💻  Send requests to http://%s:%s', config.apiHost, config.apiPort);
  });

  io.on('connection', (socket) => {
    socket.emit('news', {msg: `'Hello World!' from server`});

    socket.on('history', () => {
      for (let index = 0; index < bufferSize; index++) {
        const msgNo = (messageIndex + index) % bufferSize;
        const msg = messageBuffer[msgNo];
        if (msg) {
          socket.emit('msg', msg);
        }
      }
    });

    socket.on('msg', (data) => {
      data.id = messageIndex;
      messageBuffer[messageIndex % bufferSize] = data;
      messageIndex++;
      io.emit('msg', data);
    });
  });
  io.listen(runnable);
})
} else {
  console.error('==>     ERROR: No PORT environment variable has been specified');
}
