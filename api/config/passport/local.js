var LocalStrategy = require('passport-local').Strategy;
var models = require('../../db/models');

// When passport.authenticate('local') is used, this function will receive
// the email and password to run the actual authentication logic.
var strategyFn = function (email, password, done) {
  process.nextTick(function(){
    models.User.find({
      where: {email: email},
      include: [{
        model: models.AccountTypes,
      }]
    })
    .then(function(user){
      if(!user)
        // if the user is not exist
        return done(null, false, {message: "The user does not exist"});
      else if(!user.comparePassword(password))
        // if password does not match
        return done(null, false, {message: "Wrong password"});
      else
        // if everything is OK, return null as the error
        // and the authenticated user
        return done(null, user);

    })
    .error(function(err){
      // if command executed with error
      return done(err);
    });
  })
};

/*
 By default, LocalStrategy expects to find credentials in parameters named username and password.
 If your site prefers to name these fields differently, options are available to change the defaults.
 */
module.exports = new LocalStrategy({ usernameField: 'email', passwordField: 'password' }, strategyFn)
