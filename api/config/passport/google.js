var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var models = require('../../db/models');
var crypto = require('crypto');

var generateSalt = function() {
  return crypto.randomBytes(16).toString('base64');
};

var googleCredentials = {
        clientID: '1013530496895-mm9n2h9ddmu97kufg8v3jqihadobenug.apps.googleusercontent.com',
        clientSecret: 'hBNKqIPX94IhjhHGLDhdZjK7',
        callbackURL: 'http://localhost:3030/auth/google/callback'
};

var verifyCallback = function (accessToken, refreshToken, profile, done) {
  process.nextTick(function(){
    models.User.find({ where: { googleId: profile.id }})
          .then(function(user){
            if (user) {
                done(null, user);
            } else {
                models.User.create({
                    firstname: profile.name.givenName,
                    lastname: profile.name.familyName,
                    email: profile.emails[0].value,
                    password: generateSalt(),
                    confirmEmail: true,
                    googleId: profile.id
                }).then(function (user) {
                    done(null, user);
                }, function (err) {
                    console.error('Error creating user from Google authentication', err);
                    done(err);
                });
            }
          }).catch(function(err){
            return done(err);
          })
  })
};
/*
 By default, LocalStrategy expects to find credentials in parameters named username and password.
 If your site prefers to name these fields differently, options are available to change the defaults.
 */
module.exports = new GoogleStrategy(googleCredentials, verifyCallback);