'use strict';

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(module.filename);
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config/config.json')[env];
var db        = {};

if (config.use_env_variable) {
  var sequelize = new Sequelize(process.env[config.use_env_variable]);
} else {
  var sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(function(file) {
    var model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});



(function(m) {
  m.User.hasMany(m.Comments, {foreignKey: 'commenter_ID'});
  m.Comments.belongsTo(m.User, {foreignKey: 'commenter_ID'});

  m.User.hasMany(m.Votes, {foreignKey: 'user_ID'});
  m.Votes.belongsTo(m.User, {foreignKey: 'userID'});

  m.Categories.belongsToMany(m.User, {through: 'Categories_User'});
  m.User.belongsToMany(m.Categories, {through: 'Categories_User'});

  m.Calls.belongsTo(m.User, {foreignKey: 'AdvisorId'});
  m.Calls.belongsTo(m.User, {foreignKey: 'UserId'});


  m.Orders.belongsTo(m.Calls);
  m.Orders.belongsTo(m.User);
  m.PhoneNumber.hasMany(m.User);
  m.User.belongsTo(m.PhoneNumber);

  m.AccountTypes.hasMany(m.User);
  m.User.belongsTo(m.AccountTypes);

  m.User.hasOne(m.UserCalendar);
  m.Orders.hasOne(m.UserCalendar);
  m.GlobalCalendar.hasMany(m.UserCalendar);
  m.UserCalendar.belongsTo(m.GlobalCalendar);
})(db);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
