"use strict";

module.exports = function(sequelize, DataTypes) {
  var GlobalCalendar = sequelize.define("GlobalCalendar", {
    utc: {
      type: DataTypes.BIGINT,
      allowNull : false
    },

    formatted: {
      type: DataTypes.DATE,
      allowNull : false
    }
  });

  return GlobalCalendar;
};
