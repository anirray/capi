"use strict";

module.exports = function(sequelize, DataTypes) {
  var Votes = sequelize.define("Votes", {
    vote: DataTypes.INTEGER,
    created_at: DataTypes.DATE

  });

  return Votes;
};