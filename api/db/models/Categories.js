"use strict";

module.exports = function(sequelize, DataTypes) {
  var Categories = sequelize.define("Categories", {
    name: {
      type: DataTypes.STRING,
      unique: true
    }
  });

  return Categories;
};
