module.exports = function(sequelize, DataTypes) {
  var PhoneNumber = sequelize.define("PhoneNumber", {
    phoneNumber: {
      type: DataTypes.STRING(20)
    },
    confirmPhone: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    location: DataTypes.STRING,
    timezone: DataTypes.INTEGER,
  });

  return PhoneNumber;
};