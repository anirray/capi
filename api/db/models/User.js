"use strict";

var crypto = require('crypto');

// generateSalt, encryptPassword and the pre 'save' and 'correctPassword' operations
// are all used for local authentication security.
var generateSalt = function() {
  return crypto.randomBytes(16).toString('base64');
};

var encryptPassword = function(plainText, salt) {
  var hash = crypto.createHash('sha1');
  hash.update(plainText);
  hash.update(salt);
  return hash.digest('hex');
};

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define("User", {
    firstname: {
      type: DataTypes.STRING,
      allowNull : false
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull : false
    },
    password: {
      type: DataTypes.STRING,
      allowNull : false
    },
    salt: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull : false,
      validate: {
        isEmail: true
      }
    },
    confirmEmail: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    phoneNumber: {
      type: DataTypes.STRING(20)
    },
    phoneConfirmation: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    phoneConfirmationPin: {
      type: DataTypes.INTEGER
    },
    rate: {
      type: DataTypes.FLOAT
    },
    timezone: DataTypes.STRING,
    googleId: DataTypes.STRING,
    description: DataTypes.STRING,
    s3pictureurl: DataTypes.STRING,
    linkedInToken: DataTypes.STRING,
    stripeToken: DataTypes.STRING,
    stripeKey: DataTypes.STRING,
    twilioToken: DataTypes.STRING
  }, {
    instanceMethods : {
      comparePassword: function(candidatePassword) {
        return encryptPassword(candidatePassword, this.salt) === this.password;
      },
      changePassword: function(password) {
        return encryptPassword(password, this.salt);
      }
    }
  });

  User.beforeCreate(function(user, options) {
    user.salt = generateSalt();
    user.password = encryptPassword(user.password, user.salt);
    return sequelize.Promise.resolve(user);
  });

  return User;
};
