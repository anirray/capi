"use strict";

module.exports = function(sequelize, DataTypes) {
  var Calls = sequelize.define("Calls", {
    duration: DataTypes.BIGINT,
    state: DataTypes.STRING,
    conferenceNumber: DataTypes.STRING,
    bookingID: DataTypes.STRING,
    advisorSID: DataTypes.STRING,
    userSID: DataTypes.STRING
  });

  return Calls;
};
