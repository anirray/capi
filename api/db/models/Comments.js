"use strict";

module.exports = function(sequelize, DataTypes) {
  var Comments = sequelize.define("Comments", {
    text: DataTypes.STRING
  });

  return Comments;
};