'use strict';

module.exports = function(sequelize, DataTypes) {
  const UserCalendar = sequelize.define('UserCalendar', {
    state: {
      type: DataTypes.STRING,
      allowNull: false
    }
  });

  return UserCalendar;
};
