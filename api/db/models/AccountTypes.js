"use strict";

module.exports = function(sequelize, DataTypes) {
  var AccountTypes = sequelize.define("AccountTypes", {
  	name: {
      type: DataTypes.STRING,
      unique: true
    }
  });

  return AccountTypes;
};
