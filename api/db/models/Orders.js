module.exports = function(sequelize, DataTypes) {
  var Orders = sequelize.define("Orders", {
    amount: DataTypes.INTEGER,
    capture: DataTypes.BOOLEAN,
    state: DataTypes.STRING,
    chargeID: DataTypes.STRING
  });

  return Orders;
};
