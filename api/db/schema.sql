CREATE TABLE "User" (
	"ID" int NOT NULL,
	"First Name" char NOT NULL,
	"Last Name" char NOT NULL,
	"accountType" int NOT NULL DEFAULT '1(user)',
	"phoneNumber" varchar NOT NULL,
	"description" TEXT NOT NULL,
	"s3pictureurl" varchar NOT NULL,
	"linkedInToken" varchar NOT NULL,
	"stripeToken" varchar NOT NULL,
	"payPalToken" varchar NOT NULL,
	"twilioToken" varchar NOT NULL,
	CONSTRAINT User_pk PRIMARY KEY ("ID")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "AccountType" (
	"id" int NOT NULL,
	"name" int NOT NULL,
	CONSTRAINT AccountType_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "PhoneNumber" (
	"id" int NOT NULL,
	"user_id" int NOT NULL,
	"phone_number" varchar NOT NULL,
	"location" TEXT NOT NULL,
	"timezone" varchar NOT NULL
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Voting" (
	"id" int NOT NULL,
	"vote" int NOT NULL DEFAULT '0',
	"user_id" int NOT NULL,
	"voter_id" int NOT NULL,
	"created_at" DATE NOT NULL,
	CONSTRAINT Voting_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Transactions/Calls" (
	"id" bigint NOT NULL,
	"duration" bigint NOT NULL,
	"caller" bigint NOT NULL,
	"callee" bigint NOT NULL,
	"rate" int NOT NULL,
	"created_at" DATE NOT NULL,
	CONSTRAINT Transactions/Calls_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Categories" (
	"id" int NOT NULL,
	"name" varchar NOT NULL
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Categories_User" (
	"user_id" int NOT NULL,
	"category_id" int NOT NULL
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Comments" (
	"id" bigint NOT NULL,
	"text" TEXT NOT NULL,
	"commenter_Id" int NOT NULL,
	"page_id" int NOT NULL,
	CONSTRAINT Comments_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



ALTER TABLE "User" ADD CONSTRAINT "User_fk0" FOREIGN KEY (accountType) REFERENCES AccountType(id);
ALTER TABLE "User" ADD CONSTRAINT "User_fk1" FOREIGN KEY (phoneNumber) REFERENCES PhoneNumber(id);


ALTER TABLE "PhoneNumber" ADD CONSTRAINT "PhoneNumber_fk0" FOREIGN KEY (user_id) REFERENCES User(ID);

ALTER TABLE "Voting" ADD CONSTRAINT "Voting_fk0" FOREIGN KEY (user_id) REFERENCES User(ID);
ALTER TABLE "Voting" ADD CONSTRAINT "Voting_fk1" FOREIGN KEY (voter_id) REFERENCES User(ID);

ALTER TABLE "Transactions/Calls" ADD CONSTRAINT "Transactions/Calls_fk0" FOREIGN KEY (caller) REFERENCES User(ID);
ALTER TABLE "Transactions/Calls" ADD CONSTRAINT "Transactions/Calls_fk1" FOREIGN KEY (callee) REFERENCES User(ID);


ALTER TABLE "Categories_User" ADD CONSTRAINT "Categories_User_fk0" FOREIGN KEY (user_id) REFERENCES User(ID);
ALTER TABLE "Categories_User" ADD CONSTRAINT "Categories_User_fk1" FOREIGN KEY (category_id) REFERENCES Categories(id);

ALTER TABLE "Comments" ADD CONSTRAINT "Comments_fk0" FOREIGN KEY (commenter_Id) REFERENCES User(ID);
ALTER TABLE "Comments" ADD CONSTRAINT "Comments_fk1" FOREIGN KEY (page_id) REFERENCES User(ID);

