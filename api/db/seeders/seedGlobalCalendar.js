'use strict';

const moment = require('moment-timezone');
const models = require('../models');
const timeArr = [];




models.sequelize.sync({
    force: true
  })
  .then(() => {
    console.log('DB Cleared!');
    for (var i = moment("2016/05/08 12:00").utc().valueOf(); i <= moment("2016/05/08 12:00").utc().add(1, 'months').valueOf(); i += 900000) {
      timeArr.push({
        utc: moment(i).valueOf(),
        formatted: moment(i).format()
      });
    }
    models.GlobalCalendar.bulkCreate(timeArr)
      .then(() => {
        console.log('Global Calendar seeded');
      })
      .catch(err => {
        console.log('Error during seed', err);
      });
  });
