var models = require('../models');
var roles = [
  {name: 'User'},
  {name: 'Advisor'},
  {name: 'Admin'}
];

models.sequelize.sync().then(() => {
  return models.AccountTypes.bulkCreate(roles);
}).then((createdRoles) => {
  console.log('Created Roles #', createdRoles.length)
}).catch((err) => {
  console.log(err);
})
