var models = require(__dirname + '/../../db/models');
const twilio = require('../third_party_apis/twilio.js');

export default function createUser(req) {
  return new Promise((resolve, reject) => {
    const {newPhoneNumber} = req.body;
    const phoneConfirmationPin = Math.floor(Math.random()*90000) + 10000;
    models.User.find({
      where: {'id' : req.user.id}
    }).then((user) => {
      if(!user) reject('No User Found')
      return user.update({phoneNumber: newPhoneNumber, phoneConfirmation: false, phoneConfirmationPin: phoneConfirmationPin})
        .then((user) => {
          return twilio.sendText(user.phoneNumber, "9177220480", `This is your confirmation pin: ${user.phoneConfirmationPin}`)
            .then(() => resolve(user));
        });
    }).catch((err) => {
      reject(err);
    });
  })
}
