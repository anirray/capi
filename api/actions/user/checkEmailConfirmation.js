var models = require(__dirname + '/../../db/models');
import jwt from 'jsonwebtoken';

export default function checkEmailConfirmation(req) {
  return new Promise((resolve, reject) => {
    let token = req.query.token;

    let secret = 'optimusprimeismyrealdad'
    jwt.verify(token, secret, function(err, decoded) {
      if(err) reject(err);
      else {
      models.User.find({
        where: {'id' : decoded.id }
      }).then((user) => {
        if(!user) reject('No User Found')
        if(user.confirmEmail === true) reject('This account has already been confirmed')
        return user.update({
          confirmEmail: true
        }).then(() => {
          resolve();
          return null;
        })
      }).catch((err) => {
        reject(err);
      });
      }
    });
  })
}