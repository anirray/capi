var models = require(__dirname + '/../../db/models');

export default function createUser(req) {
  return new Promise((resolve, reject) => {

    models.User.find({
      where: {'id' : req.user.id}
    }).then((user) => {
      if(!user) reject('No User Found')
      return user.update({...req.body})
      .then((user) => {
          resolve(user);
      });
      return null;
    }).catch((err) => {
      reject(err);
    });
  })
}
