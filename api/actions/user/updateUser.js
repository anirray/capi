var models = require(__dirname + '/../../db/models');

export default function updateUser(req,params) {
  return new Promise((resolve,reject) => {
    let userId = +params[0];

    models.User
    .findById(userId)
    .then(function(user) {
      if(!user) reject('No User Found')
      return user
          .updateAttributes(req.body, {
            fields: ['username', 'email']
          })
          .then(function(updatedUser, err) {
            resolve(updatedUser);
            return null;
          })
    }).catch((error) => {
      reject(error);
    });

  })
}
