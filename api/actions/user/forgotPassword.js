import passport from 'passport';
import sendTemplate from '../../utils/mandrillPromise';
import jwt from 'jsonwebtoken';
var models = require(__dirname + '/../../db/models');

export default function forgotPassword( req, res, next ) {

    return new Promise( ( resolve, reject ) => {

          let email = req.query.email;

          let secret = 'optimusprimeismyrealdad'

          models.User.find({
            where: {'email' : email}
          }).then((user) => {
            if(!user) reject ('No User with this Email Found')
            let token = jwt.sign({ email, id: user.id }, secret, {expiresIn: '3 days'});
            sendTemplate({
              "template_name": 'cognito-forgotpassword',
              "template_content": [
                  {
                      "name": "example name",
                      "content": "example content"
                  }
              ],
              "message": {
                "subject": "Forgot Password - The Cognito",
                "from_email": "team@thecognito.com",
                "from_name": "The Cognito",
                "to": [{
                        "email": email,
                        "name": "Recipient Name",
                        "type": "to"
                    }],
                "global_merge_vars": [
                  {
                          "name": "firstname",
                          "content": user.firstname
                  },
                  {
                      "name": "forgotpasswordlink",
                      "content": "http://localhost:3000/confirmpassword?token="+token,
                  }
              ],
                "headers": {
                    "Reply-To": "team@thecognito.com"
                },
              }
            })
            resolve();
            return null;
          }).catch((err) => {
            err.message = 'No User with this Email Found';
            reject(err);
          });
    })
}