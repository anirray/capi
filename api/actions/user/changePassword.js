var models = require(__dirname + '/../../db/models');

export default function createUser(req) {
  return new Promise((resolve, reject) => {
    const {password, newPassword, confirmPassword } = req.body;

    if(newPassword !== confirmPassword) {
      reject('Passwords do not match')
    }

    models.User.find({
      where: {'id' : req.user.id}
    }).then((user) => {
      if(!user) reject('No User Found')
      if(!user.comparePassword(password)) {
        reject('Incorrect Password')
      } else {
        return user.update({password: user.changePassword(newPassword)})
        .then((user) => {
            resolve();
        });
      }
      return null;
    }).catch((err) => {
      reject(err);
    });
  })
}
