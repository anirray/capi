import passport from 'passport';
import sendTemplate from '../../utils/mandrillPromise';
import jwt from 'jsonwebtoken';
const models = require(__dirname + '/../../db/models');
const stripe = require('../third_party_apis/stripe.js');
const twilio = require('../third_party_apis/twilio.js');
export default function signup(req, res, next) {

  return new Promise((resolve, reject) => {

    var newUser = req.body;
    var user = null;


    // stripe.listAccounts()
    //   .then((accounts, err) => {
    //     accounts.data.forEach(acct => {
    //       if(acct.email === newUser.email)
    //     });
    //   })

    function createUser(inputtedUser, stripeAccountId) {
      return models.User.create({
          firstname: inputtedUser.firstname,
          lastname: inputtedUser.lastname,
          password: inputtedUser.password,
          email: inputtedUser.email,
          phoneNumber: inputtedUser.phoneNumber,
          phoneConfirmation: false,
          phoneConfirmationPin: Math.floor(Math.random()*90000) + 10000,
          timezone: inputtedUser.timezone,
          stripeToken: stripeAccountId,
          AccountTypeId: 1, //Default User Account Type Id, Ensure to run Seed File so AccountType are populated
          rate: 20 // needs to be set in acct settings, Store
        })
        .then((user) => {

          return twilio.sendText(user.phoneNumber, "9177220480", `This is your confirmation pin: ${user.phoneConfirmationPin}`)
            .then(() => {return user; });
        })
        .then((user) => {
          return req.logIn(user, (err) => {
            if (err) reject(err);
            else {
              let secret = 'optimusprimeismyrealdad'
              let token = jwt.sign({
                id: user.id
              }, secret);

              sendTemplate({
                "template_name": 'cognito-signup',
                "template_content": [{
                  "name": "example name",
                  "content": "example content"
                }],
                "message": {
                  "subject": "Please confirm your e-mail address - The Cognito",
                  "from_email": "team@thecognito.com",
                  "from_name": "The Cognito",
                  "to": [{
                    "email": user.email,
                    "name": "Recipient Name",
                    "type": "to"
                  }],
                  "global_merge_vars": [{
                    "name": "firstname",
                    "content": user.firstname
                  }, {
                    "name": "profilelink",
                    "content": "http://localhost:3000/confirmuser?token=" + token,
                  }],
                  "headers": {
                    "Reply-To": "team@thecognito.com"
                  },
                }
              });

              resolve(user);
              return null;
            }
          });
        });
      return null;
    }


    stripe.listAccounts()
      .then((accounts, err) => {
        if (accounts.data.map(function(e) {
            return e.email;
          }).indexOf(newUser.email) >= 0) {
          const stripeID = accounts.data[accounts.data.map(function(e) {
            return e.email;
          }).indexOf(newUser.email)].id;
          return createUser(newUser, stripeID)
        } else {
          return stripe.createAccount(newUser)
            .then(stripeAccount => {
              return createUser(newUser, stripeAccount.id)
            })
        }
      })
      .catch((err) => {
        console.log('err', err);
        reject(err);
      });
  })
}
