var models = require(__dirname + '/../../db/models');

export default function deleteUser(req, params) {
  return new Promise((resolve, reject) => {
    var userId = +params[0];
    models.User
      .findById(userId)
      .then((user) => {
        if(!user) reject('No User Found');
        return user.destroy().then((response) => {
          resolve(user);
          return null;
        });
      })
      .catch((err) => {
        reject(err);
      });
  })
}