var models = require(__dirname + '/../../db/models');
var _ = require("lodash");

export default function getUser(req, params) {
  return new Promise((resolve, reject) => {

    let userId = +params[0];

    models.User.find({
      where: {'id' : userId},
      include: [{
        model: models.AccountTypes,
      }]
    }).then((user) => {
      if(!user) reject('No User Found')
      resolve(_.omit(user.toJSON(), ['password', 'salt']));
      return null;
    }).catch((err) => {
      reject(err);
    });

  });
}
