import sendTemplate from '../../utils/mandrillPromise';
import jwt from 'jsonwebtoken';

export default function signup( req, res, next ) {

    return new Promise( ( resolve, reject ) => {

                let secret = 'optimusprimeismyrealdad'
                let token = jwt.sign({ id: req.user.id }, secret);

                // Something seems to be wrong here as then chain doesn't work
                sendTemplate({
                  "template_name": 'cognito-resendEmailConfirmation',
                  "template_content": [
                      {
                          "name": "example name",
                          "content": "example content"
                      }
                  ],
                  "message": {
                    "subject": "Email Confirmation Request - The Cognito",
                    "from_email": "team@thecognito.com",
                    "from_name": "The Cognito",
                    "to": [{
                            "email": req.user.email,
                            "name": "Recipient Name",
                            "type": "to"
                        }],
                    "global_merge_vars": [
                      {
                          "name": "firstname",
                          "content": req.user.firstname
                      },
                      {
                          "name": "confirmemaillink",
                          "content": "http://localhost:3000/confirmuser?token="+token,
                      }
                  ],
                    "headers": {
                        "Reply-To": "team@thecognito.com"
                    },
                  }
                });
              resolve();
    })
}