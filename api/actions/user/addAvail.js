const moment = require('moment-timezone');
const models = require(__dirname + '/../../db/models');
import * as _ from 'underscore'
export default function addAvail(req, params) {
  let availArr = req.body.avail;
  let user = req.body.user;
  return new Promise((resolve, reject) => {
      const promises = [];
        availArr.forEach(avail =>{
          promises.push(models.GlobalCalendar.find({
          where: {utc: parseInt(avail, 10)},
          raw: true
        }));
      })

      return Promise.all(promises)
      .then(events => {
        const promises = [];
        events.forEach(event => {
          promises.push(models.UserCalendar.create({
            GlobalCalendarId: event.id,
            state: 'available',
            UserId: user.id
          }, {
            raw: true
          }))
        })

        return Promise.all(promises)
      })
      .then(createdAvails => {
        const promises = [];
        createdAvails.forEach(createdAvail => {
          promises.push(models.GlobalCalendar.find({
            where: {id: createdAvail.id},
            raw: true
          }))
        })

        return Promise.all(promises)
      })
      .then(times => {
        resolve(times);
      })
      .catch(err => {
        reject(err);
      });
    });
}
