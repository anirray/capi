// DEPRECATED NO LONGER IN USE
// NO LONGER IN USE
// NO LONGER IN USE, DO NOT USE THIS, WILL BE DELETED
var models = require(__dirname + '/../../db/models');

export default function createUser(req) {
  return new Promise((resolve, reject) => {
    let newUser = req.body;

    models.User.create(newUser).then(function(user){
      resolve(user);
    })
    .catch((err) => {
      reject(err);
    })
  })
}