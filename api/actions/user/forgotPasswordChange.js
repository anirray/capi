var models = require(__dirname + '/../../db/models');
import jwt from 'jsonwebtoken';

export default function createUser(req) {
  return new Promise((resolve, reject) => {
    const {password, token } = req.body;

    let secret = 'optimusprimeismyrealdad';
    jwt.verify(token, secret, function(err, decoded) {
      if(err) reject(err);
      return models.User.find({
        where: {'id' : decoded.id }
      }).then((user) => {
        if(!user) reject('No User Found')
        return user.update({password: user.changePassword(password)}).then(() => {
          resolve();
        })
      }).catch((err) => {
        reject(err);
      });
    });
  });
}
