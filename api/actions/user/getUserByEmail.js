var models = require(__dirname + '/../../db/models');
var _ = require("lodash");

export default function getUserByEmail(req, params) {
  return new Promise((resolve, reject) => {

    let email = params[0];

    models.User.find({
      where: {'email' : email}
    }).then((user) => {
      if(!user) resolve ('No User Found')
      reject(_.omit(user.toJSON(), ['password', 'salt']));
      return null;
    }).catch((err) => {
      resolve(err);
    });

  });
}
