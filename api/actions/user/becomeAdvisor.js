var models = require(__dirname + '/../../db/models');

export default function createUser(req) {
  return new Promise((resolve, reject) => {
    const {rate} = req.body;

    if (!req.user.confirmEmail) {
      reject('Please confirm your email before attempting to become an advisor');
    }
    if(!req.user.phoneConfirmation) {
      reject('Please confirm your phone number before attempting to become an advisor');
    }
    models.User.find({
      where: {'id' : req.user.id}
    }).then((user) => {
      if(!user) reject('No User Found')
        return user.update({rate: rate, AccountTypeId: 2})
        .then((user) => {
            resolve(user);
        });
    }).catch((err) => {
      reject(err);
    });
  })
}
