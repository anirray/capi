var models = require(__dirname + '/../../db/models');
import sendTemplate from '../../utils/mandrillPromise';
import jwt from 'jsonwebtoken';


export default function createUser(req) {
  return new Promise((resolve, reject) => {
    const {newEmail} = req.body;

    let secret = 'optimusprimeismyrealdad'
    let token = jwt.sign({
      id: req.user.id
    }, secret);

    models.User.find({
      where: {'id' : req.user.id}
    }).then((user) => {
      if(!user) reject('No User Found')
      return user.update({email: newEmail, confirmEmail:false})
        .then((user) => {
          sendTemplate({
            "template_name": 'cognito-changeemail',
            "template_content": [{
              "name": "example name",
              "content": "example content"
            }],
            "message": {
              "subject": "Please confirm your e-mail address - The Cognito",
              "from_email": "team@thecognito.com",
              "from_name": "The Cognito",
              "to": [{
                "email": user.email,
                "name": "Recipient Name",
                "type": "to"
              }],
              "global_merge_vars": [{
                "name": "firstname",
                "content": user.firstname
              }, {
                "name": "profilelink",
                "content": "http://localhost:3000/confirmuser?token=" + token,
              }],
              "headers": {
                "Reply-To": "team@thecognito.com"
              },
            }
          });
          resolve(user);
        });
    }).catch((err) => {
      reject(err);
    });
  })
}
