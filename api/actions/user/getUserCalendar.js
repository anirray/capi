'use strict';
import * as _ from 'underscore'

const models = require(__dirname + '/../../db/models');
const moment = require('moment-timezone');

export default function getUserCalendar(req, params) {
  return new Promise((resolve, reject) => {
    let UserId = +params[0];
    return models.UserCalendar.findAll({
        where: {
          UserId: UserId
        },
        include: models.GlobalCalendar,
        raw: true
      })
      .then(events => {
        resolve(events.map(event => {
          return {
            id: event['GlobalCalendar.id'],
            utc: event['GlobalCalendar.utc'],
            formattted: event['GlobalCalendar.formatted'],
            createdAt: event['GlobalCalendar.createdAt'],
            updatedAt: event['GlobalCalendar.updatedAt']
          }
        }))
      })
      .catch(err => {
        reject(err);
      });
  });
}
