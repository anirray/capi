var models = require(__dirname + '/../../db/models');
import jwt from 'jsonwebtoken';

export default function checkForgotPassword(req) {
  return new Promise((resolve, reject) => {
    let token = req.query.token;

    let secret = 'optimusprimeismyrealdad'
    jwt.verify(token, secret, function(err, decoded) {
      if(err) reject(err);
      else {
      models.User.find({
        where: {'id' : decoded.id }
      }).then((user) => {
        if(!user) reject('No User Found')
        resolve();
      }).catch((err) => {
        reject(err);
      });
      }
    });
  })
}