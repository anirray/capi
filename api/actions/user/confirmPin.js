var models = require(__dirname + '/../../db/models');

export default function confirmPin(req) {
  return new Promise((resolve, reject) => {
    let pin = +req.body.pin;
    let userId = req.user.id;

    models.User.find({
      where: {'id' : userId}
    }).then((user) => {
      if(user.phoneConfirmationPin !== pin) reject('Pins did not match');
      return user.update({phoneConfirmation: true}).then((user) => {
        resolve(user);
      })
    }).catch((err) => {
      reject(err);
    })
  });
}
