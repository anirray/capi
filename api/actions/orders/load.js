var models = require(__dirname + '/../../db/models');

export default function loadUsers(req) {
  return new Promise((resolve, reject) => {
    models.Orders.findAll({
      where: {'UserId' : req.user.id},
    }).then((orders) => {
      resolve(orders);
      return null;
    }).catch((err) => {
      reject(err);
    });
  });
}
