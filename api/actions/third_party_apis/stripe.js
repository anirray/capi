const stripe = require('stripe')('sk_test_USwn3ERstY7QdWzVY5L5BOE0');
const models = require(__dirname + '/../../db/models');

function createCharge(amount, currency, tokenID, description, advisor){
  return stripe.charges.create({
    amount: amount, // amount in cents
    currency: 'usd',
    capture: false, // saving the charge for later, uncaptured payments expire in 7 days
    source: tokenID,
    description: description,
    application_fee: Math.round(amount * (1/5)), // 20% fee Cognito Charges for service
    destination: advisor.stripeToken
  });
}

function captureCharge(chargeID){
  return stripe.charges.capture(chargeID);
}



function createAccount(user){
  return stripe.accounts.create({
    managed: false, //unmanaged account means user handles withdrawing money through their own stripe dashboard
    country: 'US',
    email: user.email
  })
}

function listAccounts(){
  return stripe.accounts.list({limit: 100})
}

function accountBalance(){ //admin only
  return stripe.balance.retrieve()
    .then(balance => {
      return balance.available[0].amount
    })
}


module.exports = {
  createCharge: createCharge,
  captureCharge: captureCharge,
  createAccount: createAccount,
  listAccounts: listAccounts,
  accountBalance: accountBalance
}
