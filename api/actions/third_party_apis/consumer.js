'use strict';


const kue = require('kue');
const queue = kue.createQueue();
const models = require(__dirname + '/../../db/models');
const twilio = require('./twilio.js');


queue.process('scheduleCall', function(job, done){
  twilio.createConference(job.data.userNum, job.data.advisorNum, job.data.twilioNum, job.data.pin)
    .then(conference => {
      models.Calls.update({status: 'active', userSID: conference[0].sid, advisorSID: conference[1].sid}, {where: {id: job.data.call.id}})
        .then(() => {
          done();
        })
        .catch(err => {
          done(err);
        });
    });
});


queue.process('endCall', function(job, done){
  const promises = [];
  models.Calls.find({where: {id: job.data.call.id}})
  .then(activeCall => {
    promises.push(twilio.endCallMessage(activeCall.userSID), twilio.endCallMessage(activeCall.advisorSID));
    return Promise.all(promises)
      .then(ended => {
        return models.Calls.update({status: 'completed'}, {where: {id: job.data.call.id}})
          .then(() => {
            done();
          });
      });
  })
  .catch(err => {
    done(err)
  })
});


queue.process('textConfirmation', function(job, done){
  twilio.sendText(job.data.userNum, job.data.twilioNum, job.data.msgBody)
    .then(() => {
        done();
    })
    .catch(err => {
      done(err);
    })
});


queue.process('textConfirmationDecline', function(job, done){
  twilio.sendText(job.data.userNum, job.data.twilioNum, job.data.msgBody)
    .then(() => {
        done();
    })
    .catch(err => {
      done(err);
    })
});
