const kue = require('kue');
const queue = kue.createQueue();


// schedule call, update status
function textConfirmation(num, call){
  queue.create('textConfirmation', {
    call: call,
    userNum: num,
    twilioNum: "9177220480",
    msgBody: 'Call is confirmed. Pin 1234.'
  }).save();
}

function textConfirmationDecline(num, call){
  queue.create('textConfirmationDecline', {
    call: call,
    userNum: num,
    twilioNum: "9177220480",
    msgBody: 'Advisor has declined the meeting.'
  }).save();
}

function scheduleCall(userNum, advisorNum, time, call) {
  queue.create('scheduleCall', {
    call: call,
    userNum: userNum,
    advisorNum: advisorNum,
    twilioNum: "9177220480",
    pin: "1234"
  }).delay(new Date(time)).save();
}
// end call, update status

function endCall(userNum, advisorNum, time, call){
  queue.create('endCall', {
    call: call,
    userNum: userNum,
    advisorNum: advisorNum,
  }).delay(new Date(time)).save();
}


module.exports = {
  scheduleCall: scheduleCall,
  textConfirmation: textConfirmation,
  textConfirmationDecline: textConfirmationDecline,
  endCall: endCall
};
