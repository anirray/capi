"use strict";

const twilio = require('twilio');
const client = new twilio.RestClient('AC89d838b91f3cb67197cb5acc4b29b7e6', 'd2b01d5b65107d28301d593319630c68');



// send text
function sendText(userNum, twilioNum, msgBody){
  return client.sms.messages.create({
      to: userNum,
      from: twilioNum,
      body: msgBody
  })
}
// create conference

function createConference(userNum, advisorNum, twilioNum, pin){
  var promises = [];
  let user = {
        to: userNum,
        from: twilioNum,
        Url: `http://twimlets.com/conference?Password=${pin}&Message=Welcome%20to%20Cognito.%20Please%20wait%20for%20your%20advisor%20to%20enter%20the%20conference`

  };

  let advisor = {
      to: advisorNum,
      from: twilioNum,
      Url: `http://twimlets.com/conference?Password=${pin}&Message=Welcome%20to%20Cognito.%20Please%20wait%20for%20your%20advisor%20to%20enter%20the%20conference`
  };

  promises.push(client.makeCall(user), client.makeCall(advisor));

  return Promise.all(promises);

}



function endCall(CallSID){
        return client.calls(CallSID).update({
          status: "completed"
  });
}

function callPlayMessage(msg, CallSID){
  let twilioMsg = msg.split(" ").join("+");
  return client.calls(CallSID).update({
    url: `http://twimlets.com/message?Message[0]=${twilioMsg}`,
    method: "POST"
})

}

function endCallMessage(CallSID){
  return client.calls(CallSID).update({
      url: 'http://twimlets.com/message?Message%5B0%5D=Your%20call%20is%20ending%20now.%20Thank%20you%20for%20using%20Cognito&',
      method: "POST"
  })
}



// get active calls
function getActiveCalls(){
  return client.calls.list({ status: "active"});
}
// get all calls

function getAllCalls(){
  return client.calls.list();
}

// list conferences
function getConferences(){
  return client.conferences.list();
}


module.exports = {
  sendText: sendText,
  createConference: createConference,
  endCall: endCall,
  callPlayMessage: callPlayMessage,
  getActiveCalls: getActiveCalls,
  getAllCalls: getAllCalls,
  getConferences: getConferences
}


// list conference participants
