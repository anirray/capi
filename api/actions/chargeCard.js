const stripe = require("./third_party_apis/stripe");
const twilio = require("./third_party_apis/twilio");
const moment = require("moment-timezone");
const producer = require("./third_party_apis/producer");
import sendTemplate from '../utils/mandrillPromise';
const models = require(__dirname + '/../db/models');

export default function chargeCard(req) {
  let token = req.body.token;
  let amount = req.body.amount;
  let times = req.body.times;
  let user = req.body.user;
  let advisor = req.body.advisor;
  return new Promise((resolve, reject) => {
    return models.Orders.create({
        state: 'tentative'
      }, {
        raw: true
      })
      .then(order => {
        const promises = [];
        times.forEach(time => {
          promises.push(models.GlobalCalendar.find({
            where: {
              utc: time
            },
            raw: true
          }))
        })
        return Promise.all(promises)
          .then((slots) => {
            return {
              slots: slots,
              order: order
            }
          })
      })
      .then((obj) => {
        const promises = []
        obj.slots.forEach(slot => {
          promises.push(models.UserCalendar.find({
            where: {
              GlobalCalendarId: slot.id
            },
            raw: true
          }));
        });
        return Promise.all(promises)
          .then((userEvents) => {
            return {
              userEvents: userEvents,
              order: obj.order
            }
          })
      })
      .then(obj => {
        const promises = [];
        obj.userEvents.forEach(userEvent => {
          promises.push(models.UserCalendar.update({
            state: 'tentative',
            OrderId: obj.order.id
          }, {
            where: {
              id: userEvent.id
            },
            raw: true
          }))
        });
        return Promise.all(promises)
          .then(() => {
            return obj.order
          })
      })
      .then((order) => {
        return models.Calls.create({
          duration: times.length * 15,
          conferenceNumber: '9177220480', //hardcoded
          UserId: user.id,
          AdvisorId: advisor.id,
          state: 'tentative' //update to state
        }).then((call) => {
          return {
            call: call,
            order: order
          }
        })
      })
      .then(obj => {
        return models.Orders.update({
          CallId: obj.call.id
        }, {
          where: {
            id: obj.order.id
          }
        }).then(() => {
          return obj.call
        })
      })
      .then(call => {
        const promises = [];
        promises.push(models.User.find({
          where: {
            id: call.UserId
          },
          raw: true
        }))

        promises.push(models.User.find({
            where: {
              id: call.AdvisorId
            },
            raw: true
          }))
          // call auto-schedules until confirm/decline is setup
        return Promise.all(promises).then((res) => {
          producer.textConfirmation(res[0].phoneNumber, call)
          producer.textConfirmation(res[1].phoneNumber, call)
          producer.scheduleCall(res[0].phoneNumber, res[1].phoneNumber, Math.min.apply(Math, times), call);
          producer.endCall(res[0].phoneNumber, res[1].phoneNumber, Math.max.apply(Math, times), call);
        })
      }).then(res => {
        sendTemplate({
          "template_name": 'cognito-confirmorder-user',
          "template_content": [{
            "name": "example name",
            "content": "example content"
          }],
          "message": {
            "subject": "Email Confirmation Request - The Cognito",
            "from_email": "team@thecognito.com",
            "from_name": "The Cognito",
            "to": [{
              "email": req.user.email,
              "name": "Recipient Name",
              "type": "to"
            }],
            "global_merge_vars": [{
              "name": "firstname",
              "content": user.firstname
            }, {
              "name": "advisor",
              "content": advisor.firstname
            }, {
              "name": "duration",
              "content": minutes
            }, {
              "name": "starttime",
              "content": start.toString()
            }, {
              "name": "endtime",
              "content": end.toString()
            }, {
              "name": "dashboardlink",
              "content": "http://localhost:3000/",
            }],
            "headers": {
              "Reply-To": "team@thecognito.com"
            },
          }
        });

        sendTemplate({
          "template_name": 'cognito-confirmorder-advisor',
          "template_content": [{
            "name": "example name",
            "content": "example content"
          }],
          "message": {
            "subject": "Call Confirmation Request",
            "from_email": "team@thecognito.com",
            "from_name": "The Cognito",
            "to": [{
              "email": advisor.email,
              "name": "Recipient Name",
              "type": "to"
            }],
            "global_merge_vars": [{
              "name": "firstname",
              "content": advisor.firstname
            }, {
              "name": "username",
              "content": user.firstname
            }, {
              "name": "duration",
              "content": minutes
            }, {
              "name": "starttime",
              "content": start.toString()
            }, {
              "name": "endtime",
              "content": end.toString()
            }, {
              "name": "dashboardlink",
              "content": "http://localhost:3000/",
            }],
            "headers": {
              "Reply-To": "team@thecognito.com"
            },
          }
        });

        resolve(res)
      })
      .catch(function(err) {
        reject(err);
      });
  })
}
