const models = require(__dirname + '/../db/models');

export default function globalCalendar(req) {
  return new Promise((resolve, reject) => {
    models.GlobalCalendar.findAll({
      where: {
        utc:
      {
        gt: Date.now()
      }
    },
    raw: true
    })
    .then(function(events) {
      resolve(events);
      return null;
    })
    .catch(err => {
      reject(err);
    });
  });
}
