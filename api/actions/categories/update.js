var models = require(__dirname + '/../../db/models');

export default function updateUser(req,params) {
  return new Promise((resolve,reject) => {
    models.Categories
    .findById(req.body.id)
    .then(function(category) {
      if(!category) reject('No Category Found')
      return category
          .updateAttributes({name: req.body.name})
          .then(function(updatedCategory, err) {
            resolve(updatedCategory);
            return null;
          })
    }).catch((error) => {
      reject(error);
    });

  })
}
