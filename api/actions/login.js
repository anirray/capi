import passport from 'passport';
import _ from 'lodash';

export default function login( req, res, next ) {

    return new Promise( ( resolve, reject ) => {

        passport.authenticate( 'local', function authenticate( err, user, info ){

            if( err )    return reject( err )
            if( ! user ) return reject( info )

            req.logIn( user, err => {
                if( err ) reject( err )
                else resolve( user )
            })

        })( req, res, next )
    })
}