export loadInfo from './loadInfo';
export loadAuth from './loadAuth';
export login from './login';
export logout from './logout';
export loadUser from  './user';
export chargeCard from './chargeCard';
export checkCalendar from './checkCalendar';
export globalCalendar from './globalCalendar';
export * as widget from './widget/index';
export * as categories from './categories/index';
export * as calls from './calls/index';
export * as orders from './orders/index';
export * as users from './user/index';
