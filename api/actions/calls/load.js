var models = require(__dirname + '/../../db/models');

export default function loadUsers(req) {
  return new Promise((resolve) => {
    models.Calls.findAll({
      where: {'UserId' : req.user.id},
    }).then((calls) => {
      resolve(calls);
      return null;
    }).catch((err) => {
      reject(err);
    });
  });
}
