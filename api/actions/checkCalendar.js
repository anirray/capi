var models = require(__dirname + '/../db/models');

export default function chargeCard(req) {
  var userId = req.body.user.id;
  return new Promise((resolve, reject) => {
    models.UserCalendar.findAll({
      where: {'advisorID': userId}
    })
    .then(function(events) {
      resolve(events);
    }).catch((err) =>{
      reject(err);
    });
  });
}
