const models = require(__dirname + '/../../db/models');
const producer = require("./third_party_apis/producer");
export default function appointmentConfirmation(req) {
  return new Promise((resolve, reject) => {
    let user = req.body.user; //advisor
    let apptStatus = req.body.apptStatus; //change to appointment request response
    let orderId = req.body.orderId;
    let slot = req.body.slot;
    if(apptStatus === 'confirmed'){
    return models.Orders.update({
      status: apptStatus
    }, {
      where: {id: orderId},
      raw: true,
      returning: true
    })
    .then(() => {
      return models.Orders.find({
        where: {id: orderId},
        raw: true
      })
    })
    .then(order => {
      return models.Calls.find({
        where: {id: order.CallId},
        raw: true
      })
    })
    .then(call => {
      const promises = [];
      promises.push(models.User.find({
        where: {id: call.AdvisorId},
        raw: true
      }))

      promises.push(models.User.find({
        where: {id: call.UserId},
        raw: true
      }))

      return Promise.all(promises).then((res) => {
        producer.textConfirmation(res[0].phoneNumber, call)
        producer.textConfirmation(res[1].phoneNumber, call)
        producer.scheduleCall(res[0].phoneNumber, res[1].phoneNumber, slot.start, call);
        producer.endCall(res[0].phoneNumber, res[1].phoneNumber, slot.end, call);
      })
    })
    .then(function(res){
      resolve(res); //returned user from update
    })
    .catch((err) => {
      reject(err);
    })
  } else {
    return models.Orders.update({
      status: apptStatus
    }, {
      where: {id: orderId},
      raw: true,
      returning: true
    })
    .then(() => {
      return models.Orders.find({
        where: {id: orderId},
        raw: true
      })
    })
    .then(order => {
      return models.Calls.find({
        where: {id: order.CallId},
        raw: true
      })
    })
    .then(call => {
      const promises = [];
      promises.push(models.User.find({
        where: {id: call.AdvisorId},
        raw: true
      }))

      promises.push(models.User.find({
        where: {id: call.UserId},
        raw: true
      }))

      return Promise.all(promises).then((res) => {
        producer.textConfirmationDecline(res[1].phoneNumber, call)
      })
    })
    .then(function(res){
      resolve(res); //returned user from update
    })
    .catch((err) => {
      reject(err);
    })
  }
  })
}
